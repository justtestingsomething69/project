const donnes={

    "console1":[
    
        {"ref":"Jean D98","prix":1000,"image":"JeanD98.webp"},
    
        {"ref":"Essential Heavy","prix":500,"image":"EssentialHeavy.jpg"},
    
        {"ref":"Pullover Hoody","prix":750,"image":"PulloverHoody.jpg"},
    
        {"ref":"Sweatshirts Women","prix":800,"image":"SweatshirtsWomen.jpg"},

        {"ref":"shoe polo assn","prix":1000,"image":"shoepoloassn.webp"}
    
        ]
    

    
    }
    
    function validation(console,Qtte){
    
        return (console!="choisir une console" &&  Qtte>0 && Qtte<=20);
    
    }
    
    function remplirConsole(){
    
        let cnsl= donnes.console1;
    
        cnsl.forEach(elem => {
    
        let opt=document.createElement("option")
        
        opt.innerHTML=elem.ref
        
        document.getElementById("slct").appendChild(opt)
    
    })
    
    }
    
    function addConsoleToCart(){
    
        let consoles=donnes.console1;
    
        let cons=document.getElementById("slct").value;
    
        let qtte=document.getElementById("ipt").value;
    
        let bodyt=document.getElementById("body_tbd");    
    
        if(validation(cons,qtte)){
    
    consoles.forEach(element =>{
    
    if(element.ref==cons){
    
        let tr=document.createElement("tr");
    
        let td1=document.createElement("td");
    
        let td2=document.createElement("td");
    
        let td3=document.createElement("td");
    
        let td4=document.createElement("td");
    
        labelR=document.createElement("label");
    
        labelI=document.createElement("label");
    
        labelR.innerHTML=element.ref;
    
        td1.appendChild(labelR);
    
        td2.innerHTML=qtte;
    
        td3.innerHTML="<img src="+element.image+" alt=non width='150px' >";
    
        td4.innerHTML="<button onclick='remove(event)' style='background-color:rgb(255, 74, 116);border-radius:20px;border :none;height:30px;width:100px;border-radiuse:20px'>SUPRIMER</button>"
    
        tr.appendChild(td1);
    
        tr.appendChild(td2);
    
        tr.appendChild(td3);
    
        tr.appendChild(td4);
    
        bodyt.appendChild(tr);
    
    
    
    
    }
    
    })
    }
    }

    function remove(e){
        
        var confir=confirm("Vous etes sur que vous voulez suprimer cette console ?")
        if(confir){
            tr=e.target.parentNode.parentNode
            tr.remove()
        }

    }
    function getPrix(reference){
        let prix=0
        donnes.console1.forEach(elem=>{
            if(reference==elem.ref){
                prix=elem.prix
                
            }
        })
        return prix
    }
    function calculerHT(){
        let labelHT=document.getElementById("montantHT")
        let tbody=document.getElementById("body_tbd")
        let TotalHT=0
        
        for(let i=0;i<tbody.rows.length;i++){// i=1 si je recupere la table
            let prix=getPrix(tbody.rows[i].cells[0].innerText)
            let qte=tbody.rows[i].cells[1].innerText
            TotalHT+=prix*qte

        }
        labelHT.innerHTML=TotalHT+" DH"
        return TotalHT
    }
    function calculerTTC(){
        let labelTTC=document.getElementById("montantTTC")
        let TotalTTC=calculerHT()*(1.2)
        labelTTC.innerHTML=TotalTTC+" DH"
    }
module.exports = {
        validation,
        remplirConsole,
        addConsoleToCart,
        remove,
        getPrix,
        calculerHT,
        calculerTTC,
};