// Import the functions to be tested
const {
    validation,
    remplirConsole,
    addConsoleToCart,
    remove,
    getPrix,
    calculerHT,
    calculerTTC,
  } = require('./efm1');
  
  // Mocking the necessary DOM elements using JSDOM
  const { JSDOM } = require('jsdom');
  const jsdom = new JSDOM('<body><table id="body_tbd"></table><div id="montantHT"></div></body>');
  
  global.document = jsdom.window.document;
  
  // Jest test cases
  
  test('Validation function returns true for valid input', () => {
    expect(validation('Jean D98', 5)).toBe(true);
  });
  
  test('Validation function returns false for invalid input', () => {
    expect(validation('choisir une console', 0)).toBe(false);
  });
  
  test('getPrix function returns correct price', () => {
    expect(getPrix('Jean D98')).toBe(1000);
  });
  test('remplirConsole populates dropdown list', () => {
    // Mocking document.createElement and document.getElementById
    document.createElement = jest.fn(() => ({ innerHTML: '' }));
    document.getElementById = jest.fn(() => ({ appendChild: jest.fn() }));

    remplirConsole();

    // Assert that createElement and appendChild were called
    expect(document.createElement).toHaveBeenCalled();
    expect(document.getElementById).toHaveBeenCalledWith("slct");
 });


  // Add more test cases as needed
  